export class Cell {
  public id = null;
  public status = 0; // 0 empty, -1 computer, 1 user, 2 available to pick

  constructor(cell?: Cell) {
    Object.assign(this, cell);
  }
}
