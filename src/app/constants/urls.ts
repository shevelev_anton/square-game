import { environment } from '@environments/environment';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class Urls {
  private SERVER = environment.server;
  public GAME_SETTINGS = `${this.SERVER}/game-settings`; // GET request
  public GAME_WINNERS = `${this.SERVER}/winners`; // GET POST request
}
