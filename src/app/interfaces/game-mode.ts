export interface GameMode {
  delay: number;
  field: number;
}
export interface GameModeList {
  [prop: string]: GameMode;
}
