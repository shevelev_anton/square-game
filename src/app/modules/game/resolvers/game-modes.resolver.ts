import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';
import { Urls } from '@const/urls';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GameModesResolver implements Resolve<Observable<any>> {
  constructor(private http: HttpClient, private urls: Urls) {}
  resolve(): Observable<any> {
    return this.http.get(this.urls.GAME_SETTINGS).pipe(
      map((gameModes) => {
        return gameModes;
      })
    );
  }
}
