import { TestBed } from '@angular/core/testing';

import { LeaderBordService } from './leader-bord.service';

describe('LeaderBordService', () => {
  let service: LeaderBordService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeaderBordService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
