import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GameWinner } from '@interface/game-winner';
import { GenericSubject } from 'src/app/classes/generic-subject';
import { Urls } from '@const/urls';

@Injectable({
  providedIn: 'root',
})
export class LeaderBordService {
  public winners: GenericSubject<GameWinner[]> = new GenericSubject<
    GameWinner[]
  >([]);

  constructor(private http: HttpClient, private urls: Urls) {}

  retrieveWinners(): void {
    this.http.get(this.urls.GAME_WINNERS).subscribe((winners: GameWinner[]) => {
      this.winners.value = winners;
    });
  }
  sendWinner(winner: string, date: string): void {
    this.http
      .post(this.urls.GAME_WINNERS, {
        winner,
        date,
      })
      .subscribe(() => {
        this.retrieveWinners();
      });
  }
}
