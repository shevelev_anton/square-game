import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameBoardComponent } from './components/game-board/game-board.component';
import { LeaderBoardComponent } from './components/leader-board/leader-board.component';
import { MaterialModule } from '@module/material/material.module';
import { GameComponent } from './components/game/game.component';
import { GameRoutingModule } from './game-routing.module';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [GameBoardComponent, LeaderBoardComponent, GameComponent],
  imports: [CommonModule, MaterialModule, GameRoutingModule, FormsModule],
})
export class GameModule {}
