import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './components/game/game.component';
import { GameModesResolver } from './resolvers/game-modes.resolver';

const routes: Routes = [
  {
    path: '',
    component: GameComponent,
    resolve: {
      gameModes: GameModesResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GameRoutingModule {}
