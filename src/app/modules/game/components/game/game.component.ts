import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameModeList, GameMode } from '@interface/game-mode';
import { GameService } from '@module/game/services/game.service';
import { GenericSubject } from 'src/app/classes/generic-subject';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  public winner: GenericSubject<string> = new GenericSubject<string>('');
  public objectKeys = Object.keys;
  public gameModes: GameModeList = {};
  public selectedMode: GameMode = null;
  public userName = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private gs: GameService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { gameModes: GameModeList }) => {
      this.gameModes = data.gameModes || {};
    });
    this.winner = this.gs.winner;
  }

  startGame(): void {
    this.gs.startGame(this.selectedMode, this.userName);
  }
}
