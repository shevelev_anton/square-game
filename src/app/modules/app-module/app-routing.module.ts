import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GAME } from '@module/game/game-routes';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NOT_FOUND } from './app-routes';

const routes: Routes = [
  { path: '', redirectTo: GAME, pathMatch: 'full' },
  {
    path: GAME,
    loadChildren: () => import('../game/game.module').then((m) => m.GameModule),
  },
  { path: '**', redirectTo: NOT_FOUND, pathMatch: 'full' },
  { path: NOT_FOUND, component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
